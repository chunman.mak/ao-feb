import { useFocusEffect } from '@react-navigation/native';
import React, { useCallback } from 'react';
import { Button, Text } from 'react-native';
import UserForm from '~/components/demo/UserForm';
import DefaultPage from '~/components/shells/DefaultPage';
import { useReduxDispatch, useReduxSelector } from '~/redux';
import { attemptLogin, resetLogin, selectLogin } from '~/redux/ducks/user';
import { MainRoutes } from '~/routing/routes';
import { MainNavigationProp } from '~/routing/types';

type SignInScreenProps = {
  navigation: MainNavigationProp<MainRoutes.SignIn>;
};
const SignInScreen = ({navigation}: SignInScreenProps): React.ReactElement => {
  const dispatch = useReduxDispatch();
  const isLoggedIn = useReduxSelector(selectLogin);

  useFocusEffect(
    useCallback(() => {
      if (isLoggedIn) navigation.navigate(MainRoutes.AppLoading);
    }, [navigation, isLoggedIn]),
  );

  const handleSubmit = (email: string, password: string): void => {
    dispatch(attemptLogin(email, password));
  };

  const handleSwitch = (): void => {
    dispatch(resetLogin());
    navigation.navigate(MainRoutes.SignUp);
  };

  return (
    <DefaultPage>
      <Text>Sign In</Text>
      <UserForm submitHandler={handleSubmit} label="Log in" />
      <Button title="Sign Up" onPress={handleSwitch} />
    </DefaultPage>
  );
};

export default SignInScreen;
