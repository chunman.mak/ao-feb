var glob = require('glob');

//
glob('**/*.png', null, function (er, files) {
  files.reduce((prev, cur) => {
    let name = cur;
    if (name.includes('/') > 0) {
      name = name.split('/')[name.split('/').length - 1];
    }
    return {
      ...prev,
      [name.split('.')[0]]: `require("./${cur}")`,
    };
  }, {});
});
