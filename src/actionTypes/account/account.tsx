import { createActions } from 'reduxsauce';
import type {
  AccountActionTypes,
  AccountCreators,
} from '~/types/actions/account/account';

const { Types, Creators } = createActions<AccountActionTypes, AccountCreators>({
   //
  postDeviceRegister: ['data', 'onSuccess', 'onFail'],
  postDeviceRegisterSuccess: ['data'],
  postDeviceRegisterFailure: ['error'],

  setIsPurchaseRegister: ['purchaseRegister'],
});

export const ActionTypes = Types;

export default Creators;
