import {mUatApi, mProApi} from '~/axios';


export interface apiModel {
    api: string;
    method: string;
    headerContentIV: boolean;
    contentTypeJson?: boolean;
}


export const deviceRegisterAPI: apiModel = {
    api: '/device/register',
    method: mUatApi.post,
    headerContentIV: true,
};
