import { AxiosError } from 'axios';

export default class NetworkError {
  private error: any;

  constructor(error: any) {
    this.error = error;
  }

  isAxiosError(error: any): error is AxiosError {
    return (error as AxiosError).isAxiosError !== undefined;
  }

  getMessage = () => {
    //todo
    return 'error message ';
  };

  getButtonText = () => {
    return 'error button text';
  };
}
