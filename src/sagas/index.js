import { all, fork } from 'redux-saga/effects';
//account;
import login from '~/sagas/account/account';



export default function* rootSaga() {
    yield all([
        fork(login),
    ]);
}
