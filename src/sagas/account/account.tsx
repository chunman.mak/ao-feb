import queryString from 'query-string';
import { Platform } from 'react-native';
import { all, call, fork, put, select, takeLatest } from 'redux-saga/effects';
import {
  ActionTypes,
  default as AppActions,
} from '~/actionTypes/account/account';
import apiNetworking from '~/axios/common-api';
import * as API from '~/api/config';
import NetworkError from '../NetworkError';

//-----------------------------------------------//
///watsons/my/api/v1.0/device/register
//-----------------------------------------------//
export function* postDeviceRegister(payload: any) {
  try {
    const apiModel = API.deviceRegisterAPI;
    const api = apiModel.api;
    const parameters: any = {
      platform: Platform.OS === 'ios' ? 'IOS' : 'ANDROID',
    };
    // Set updatedUUID if member card is updated
    if (payload.updatedUUID) {
      parameters.updatedUUID = payload.updatedUUID;
    }

    // @ts-ignore
    const response = yield call(
        apiNetworking.apiNetworking,
      apiModel,
      api,
      parameters,
    );

     yield put(
      AppActions.postDeviceRegisterSuccess({
        ...response.data,
      }),
    );

    if (payload.onSuccess) {
      payload.onSuccess(response.data);
    }
  } catch (error) {
    yield put(
      AppActions.postDeviceRegisterFailure({ error: new NetworkError(error) }),
    );
  }
}

export function* watchPostDeviceRegister() {
  yield takeLatest(ActionTypes.POST_DEVICE_REGISTER, postDeviceRegister);
}


export default function* login() {
  yield all([
    fork(watchPostDeviceRegister),
  ]);
}
