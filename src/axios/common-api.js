import {mUatApi, mProApi} from './index';
import {select} from 'redux-saga/effects';
import * as StoreSelectors from '~/selectors/store';
const GET = 'GET';
const POST = 'POST';
const DELETE = 'DELETE';
const PATCH = 'PATCH';
const PUT = 'PUT';

function* apiNetworking(apiData, api, parameters) {
  const user = yield select(StoreSelectors.user);
  console.log('user =', user);
  let token;
  if (user && user.data) {
    const {access_token} = user.data;
    token = access_token;
  }
  let method = POST;
  if (apiData.method === mUatApi.get || apiData.method === mProApi.get) {
    method = GET;
  } else if (
      apiData.method === mUatApi.delete ||
      apiData.method === mProApi.delete
  ) {
    method = DELETE;
  } else if (
      apiData.method === mUatApi.patch ||
      apiData.method === mProApi.patch
  ) {
    method = PATCH;
  } else if (apiData.method === mUatApi.put || apiData.method === mProApi.put) {
    method = PUT;
  }
  const header = {};
  header['Content-Type'] = 'application/json';
  header['X-Access-Token'] = token;
  return yield callAxios(apiData, api, parameters, header, method);
}

function callAxios(axios, api, params, header, method) {
  if (method === DELETE) {
    const config = {
      data: params,
      headers: header,
      withCredentials: true,
    };
    return axios.method(api, config);
  } else if (method === GET) {
    const config = {
      params,
      headers: header,
      withCredentials: true,
    };
    return axios.method(api, config);
  } else {
    return axios.method(api, params, {
      headers: header,
      withCredentials: true,
    });
  }
}



export default {
  apiNetworking,
};
