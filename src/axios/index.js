import axios from 'axios';
import {Platform} from 'react-native';
import {UAT_API_MEDIA, PRO_API_DOMAIN} from '~/provider/API';
import {handleNetworkError} from './error-interceptor';

const mUATRequest = axios.create({
  baseURL: UAT_API_MEDIA,
  timeout: 30000,
  headers: {
    'Content-Type': 'application/json',
    'User-Agent': Platform.OS === 'ios' ? 'ios' : 'Android',
  },
});

const mPRORequest = axios.create({
  baseURL: PRO_API_DOMAIN,
  timeout: 30000,
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;',
    'User-Agent': Platform.OS === 'ios' ? 'ios' : 'Android',
  },
});

function getPostConfig(params, config) {
  const defaultConfig = {
    headers: {
      'Content-Type': 'application/json',
    },
    transformRequest: function (data, requestConfig) {
      if (Object.prototype.toString.call(params) === '[object Array]') {
        return JSON.stringify(params);
      }
      return JSON.stringify(data);
    },
  };
  return Object.assign(defaultConfig, config);
}

mUATRequest.json = (url, params, config) => {
  return mUATRequest.post(url, params, getPostConfig(params, config));
};

mPRORequest.json = (url, params, config) => {
  return mPRORequest.post(url, params, getPostConfig(params, config));
};

mUATRequest.interceptors.response.use(handleNetworkError, error =>
    Promise.reject(error),
);
mPRORequest.interceptors.response.use(handleNetworkError, error =>
    Promise.reject(error),
);
export const mUatApi = Object.create(mUATRequest);
export const mProApi = Object.create(mPRORequest);
