
import { translate } from '~/translations/i18n';
import AppMessageHelper from '~/utils/AppMessageHelper';
import ShowDialogHelper from '~/utils/ShowDialogHelper';
import { isEmpty } from '~/utils/StringHelper';

const DEFAULT_ERROR_MESSAGE = translate('general_default_error_message');

const handleNetworkError = response => {
  const status = response?.data?.status;
  console.log('handleNetworkError.status.Code: ', status.code);

  if (
    (status.code !== 1000 &&
      status.code !== 1001 &&
      status.code !== 1003 &&
      status.code !== 1011 &&
      status.code !== 1012 &&
      status.code !== 1013 &&
      status.code !== 1008) ||
    status.sysMessage !== 'Success'
  ) {
    const message = status.sysMessage || DEFAULT_ERROR_MESSAGE;
    const code = status.code || 999;

    const appMsg = AppMessageHelper.getMessage(status.code);


    ShowDialogHelper.showApiError(isEmpty(appMsg) ? message : appMsg);

    return Promise.reject({
      code: code,
      message: message,
    });
  }
  return response;
};


export { handleNetworkError };
