import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { LogBox } from 'react-native';
import { enableScreens } from 'react-native-screens';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { ThemeProvider } from 'styled-components/native';
import store, { persistor } from '~/redux';
import MainNavigation from '~/routing/MainNavigation';
import theme from '~/styles/baseTheme';
LogBox.ignoreLogs(['Warning: ...']);
LogBox.ignoreAllLogs();
enableScreens();

export default function App(): React.ReactElement {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <ThemeProvider theme={theme}>
          <StatusBar hidden />
          <MainNavigation />
        </ThemeProvider>
      </PersistGate>
    </Provider>
  );
}
