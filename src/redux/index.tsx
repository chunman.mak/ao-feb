import AsyncStorage from '@react-native-async-storage/async-storage';
import { configureStore } from '@reduxjs/toolkit';
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';
import logger from 'redux-logger';
import {
  persistReducer,
  persistStore
} from 'redux-persist';
import rootMiddleware from './rootMiddleware';
import rootReducer from './rootReducer';

export type RootState = ReturnType<typeof rootReducer>;

const persistConfig = {
  key: 'root',
  version: 1,
  storage: AsyncStorage,
  whitelist: ['user'],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);
const store = configureStore({
  reducer: persistedReducer,
  devTools: process.env.NODE_ENV !== 'production',
  middleware: getDefaultMiddleware =>
    process.env.NODE_ENV !== 'production'
      ? getDefaultMiddleware({
          serializableCheck: false,
          immutableCheck: false,
        })
          .concat(logger)
          .prepend(rootMiddleware)
      : getDefaultMiddleware({
          serializableCheck: false,
          immutableCheck: false,
        }).prepend(rootMiddleware),
});

export const persistor = persistStore(store);

export type AppDispatch = typeof store.dispatch;
export const useReduxDispatch = (): AppDispatch => useDispatch<AppDispatch>();
export const useReduxSelector: TypedUseSelectorHook<RootState> = useSelector;
export default store;
