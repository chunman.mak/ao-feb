import {Action} from 'redux';
import {DeviceRegisterResponse} from '~/types/model/account/deviceRegister';
import {APIError} from '~/types/model/APIError';

export const enum TypesNames {
    POST_DEVICE_REGISTER = 'POST_DEVICE_REGISTER',
    POST_DEVICE_REGISTER_SUCCESS = 'POST_DEVICE_REGISTER_SUCCESS',
    POST_DEVICE_REGISTER_FAILURE = 'POST_DEVICE_REGISTER_FAILURE',

    SET_IS_PURCHASE_REGISTER = 'SET_IS_PURCHASE_REGISTER',
}

///api/v1.0/device/register
export interface IPostDeviceRegister
    extends Action<TypesNames.POST_DEVICE_REGISTER> {
    platform: string | null;
}

export interface IPostDeviceRegisterSuccess
    extends Action<TypesNames.POST_DEVICE_REGISTER_SUCCESS> {
    data: DeviceRegisterResponse;
}

export interface IPostDeviceRegisterFailure
    extends Action<TypesNames.POST_DEVICE_REGISTER_FAILURE> {
    error: APIError;
}

export interface ISetIsPurchaseRegister
    extends Action<TypesNames.SET_IS_PURCHASE_REGISTER> {
    purchaseRegister: boolean;
}

export interface AccountActionTypes {
    [TypesNames.POST_DEVICE_REGISTER]: string;
    [TypesNames.POST_DEVICE_REGISTER_SUCCESS]: string;
    [TypesNames.POST_DEVICE_REGISTER_FAILURE]: string;
    [TypesNames.SET_IS_PURCHASE_REGISTER]: string;
}

export interface AccountCreators {
    postDeviceRegister: (platform: string | null) => IPostDeviceRegister;

    postDeviceRegisterSuccess: (
        data: DeviceRegisterResponse,
    ) => IPostDeviceRegisterSuccess;

    postDeviceRegisterFailure: (error: APIError) => IPostDeviceRegisterFailure;

    setIsPurchaseRegister: (purchaseRegister: boolean) => ISetIsPurchaseRegister;
}
