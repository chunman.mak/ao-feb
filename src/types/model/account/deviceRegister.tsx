import Common = require('../common');

export interface DeviceInfo {
  token: string;
  expiryTime: string;
  expiryTimeStr: string;
}

export interface DeviceRegisterResponse {
  // /watsons/my/api/v1.0/device/register

  status: Common.Status;
  data: DeviceInfo;
}
