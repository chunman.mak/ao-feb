export interface Status {
  code: number;
  sysMessage: string;
  message?: string;
  systime: string;
}
