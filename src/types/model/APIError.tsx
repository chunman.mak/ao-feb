import NetworkError from '~/sagas/NetworkError';

export interface APIError {
  error: NetworkError;
}
