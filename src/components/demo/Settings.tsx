import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import React from 'react';
import {ScrollView, Text} from 'react-native';
import styled from 'styled-components/native'; // eslint-disable-line import/no-extraneous-dependencies

const Tab = createMaterialTopTabNavigator();

const TabTwo = () => (
  <ScrollView>
    <Text />
    {Array(20)
      .fill('Tab Two')
      .map((text: string, index) => {
        const tabContent = `${text} (${index})`;
        return <Text key={tabContent}>{tabContent}</Text>;
      })}
  </ScrollView>
);

const Settings = (): React.ReactElement => {
  return <TabTwo />;
};

const StyledSettingsBox = styled.View`
  background-color: #ccc;
  width: 300px;
  height: 500px;
  border-width: 4px;
  border-color: #000;
  border-radius: 5px;
`;

export default Settings;
