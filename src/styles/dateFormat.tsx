export const YYYY_MM_DD_HH_MM_SS_HYPHEN_COLON = 'YYYY-MM-DD hh:mm:ss';
export const YYYY_MM_DD_T_HH_MM_SS_HYPHEN_COLON = 'YYYY-MM-DDThh:mm:ss';
export const DD_MM_YYYY_SLASH = 'DD/MM/YYYY';
