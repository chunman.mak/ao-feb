import { Dimensions, PixelRatio } from 'react-native';
import * as Colors from './colors';
import * as Spacing from './spacing';

const { width: SCREEN_WIDTH, height: SCREEN_HEIGHT } = Dimensions.get('window');

const scale = SCREEN_WIDTH / 375;

function normalize(size) {
  const newSize = size * scale;
  return Math.round(PixelRatio.roundToNearestPixel(newSize));
}

const smallestFontSize = normalize(10);
const smallFontSize = normalize(12);
const baseFontSize = normalize(14);
const largeFontSize = normalize(24);
const extraLargeFontSize = normalize(32);
const ultraLargeFontSize = normalize(60);

// based on Zelpin design

/**
 * font from Zeplin
 */
export const zeplin = {
  font6: normalize(6),
  font7: normalize(7),
  font8: normalize(8),
  font9: normalize(9),
  font10: normalize(10),
  font11: normalize(11),
  font12: normalize(12),
  font13: normalize(13),
  font14: normalize(14),
  font15: normalize(15),
  font16: normalize(16),
  font17: normalize(17),
  font18: normalize(18),
  font20: normalize(20),
  font24: normalize(24),
  font25: normalize(25),
  font26: normalize(26),
  font28: normalize(28),
  font30: normalize(30),
  font35: normalize(35),
  font40: normalize(40),
  font50: normalize(50),
  font64: normalize(64),
  font90: normalize(90),
};

/**
 * font size
 */
export const font = {
  smallest: smallestFontSize,
  small: smallFontSize,
  base: baseFontSize,
  large: largeFontSize,
  extraLarge: extraLargeFontSize,
  ultraLarge: ultraLargeFontSize,
};

export const fontFamily = {
  regular: 'Rubik-Regular',
  bold: 'Rubik-Bold',
  medium: 'Rubik-Medium',
  light: 'Rubik-Light',
};

/**
 * vector icons
 */
export const icon = {
  base: 32,
};

export const base = {
  fontFamily: fontFamily.regular,
  fontSize: zeplin.font12,
  color: Colors.text.black,
};

export const header = {
  ...base,
  fontSize: largeFontSize,
  color: Colors.primary,
  marginVertical: Spacing.smaller,
};

export const textWhite = {
  ...base,
  color: Colors.white,
};

export const textWhite14 = {
  ...textWhite,
  fontSize: zeplin.font14,
};
