import { Dimensions } from 'react-native';
import * as Colors from './colors';
import * as Spacing from './spacing';
import * as Theme from './theme.txs';
import { iPhoneXPaddingBottom, isIphoneX } from './theme.txs';

export const base = {
  backgroundColor: Colors.black,
  justifyContent: 'center',
  alignItems: 'center',
  padding: Spacing.base,
};

/**
 * all rounded
 */
export const rounded = {
  borderRadius: Spacing.rounded.base,
};

/**
 * only top rounded
 */
export const topLeftRounded = {
  borderTopLeftRadius: Spacing.rounded.base,
};

/**
 * only bottom right rounded
 */
export const bottomRightRounded = {
  borderBottomRightRadius: Spacing.rounded.base,
};

/**
 * sticky bottom
 */
export const bottom = {
  left: Math.round(Dimensions.get('window').width * 0.11),
  right: 0,
  bottom: 0,
  paddingTop: Spacing.base,
  paddingBottom: isIphoneX ? iPhoneXPaddingBottom : Spacing.base,
  height: isIphoneX
    ? Theme.zeplinSize(50) + iPhoneXPaddingBottom
    : Theme.zeplinSize(50),
};

/**
 * Reference for calculating absolute height in screen
 */
export const bottomHeightRef = Theme.isIphoneX
  ? Theme.iPhoneXPaddingBottom * 2 + Theme.zeplinSize(50)
  : Spacing.base + Theme.zeplinSize(50);

/**
 * primary button
 */
export const primaryButton = {
  ...base,
  ...topLeftRounded,
  ...bottomRightRounded,
};

/**
 * all rounded
 */
export const roundedButton = {
  ...primaryButton,
  ...rounded,
};

/**
 * sticky bottom button
 */
export const bottomButton = {
  ...base,
  ...topLeftRounded,
  ...bottom,
};
