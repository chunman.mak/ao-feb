export function borderRadiusTopLeftBottomRight(radius: number) {
  return {
    borderTopLeftRadius: radius,
    borderBottomRightRadius: radius,
  };
}

export function borderRadiusTopCorners(radius: number) {
  return {
    borderTopLeftRadius: radius,
    borderTopRightRadius: radius,
  };
}
export function borderRadiusBottomCorners(radius: number) {
  return {
    borderBottomLeftRadius: radius,
    borderBottomRightRadius: radius,
  };
}

export function circle(height: number) {
  return {
    aspectRatio: 1,
    height: height,
    borderRadius: height / 2,
  };
}
