export const primary = '#0cd9d3';
export const secondary = '#8928d7';
export const tertiary = '#ff1481';
export const black = '#000';
export const black00 = 'rgba(0,0,0,0)';
export const black02 = 'rgba(0,0,0,0.2)';
export const black13 = 'rgba(0,0,0,0.13)';
export const black20 = 'rgba(0,0,0,0.2)';
export const black30 = 'rgba(0,0,0,0.3)';
export const black50 = 'rgba(0,0,0,0.5)';
export const black90 = 'rgba(0,0,0,0.9)';
export const white50 = 'rgba(255,255,255,0.5)';
export const white20 = 'rgba(255,255,255,0.2)';
export const white = '#FFF';
export const grey = '#f4f4f4';
export const lightGrey = '#8b8b8b';
export const secondary_100 = '#7548e6';
export const slider_pagination_black = '#4D4D4D91';
export const loginReminderBgColor = '#ffd900e6';
export const brownishGrey = '#666666';
export const lightestGrey = '#d6d6d6';
export const lighterGrey = '#f1f1f1';
export const warmGrey = '#9b9b9b';
export const separatorGrey = '#dfdfdf';
export const separatorGrey20 = 'rgba(223,223,223,0.2)';
export const tealBlue = '#009aa9';
export const liveChatRoomAgentGrey = '#eeeeee';
export const errorRed = '#ff0000';
export const bluishPurple = '#7548e6';
export const darkGrey = '#3e3e3e';
export const ratingStarYellow = '#ffd900';
export const deepSkyBlue = '#1e85e3';
export const coolGrey = '#9a9b9c';
export const deepSkyBlueTwo = '#1877f2';
export const raspberry = '#ad0050';
export const lightPink = '#fff6fa';
export const orangish = 'rgba(255, 130, 72, 0.69)';
export const strongPinkTwo = 'rgba(255, 0, 118, 0.69)';
export const warmGreyTwo = '#8b8b8b';
export const deepOrange = '#e43f00';
export const beige = '#f6dad1';
export const veryLightPink = '#ffeee8';
export const veryLightPinkTwo = '#eeeeee';
export const strongPink = '#ff1481';
export const uglyYellow = '#cdd400';
export const purpleishBlue = '#6236ff';
export const violetPink = '#ff5eff';
export const veryLightPinkThree = '#d6d6d6';
export const veryLightPinkThree30 = 'rgba(216, 216, 216, 0.3)';
export const veryLightPinkFour = '#d8d8d8';
export const veryLightPinkFour30 = '#d8d8d84d';
export const veryLightPinkSix = '#f1f1f1';
export const veryLightPinkSeven = '#e1e1e1';
export const veryLightPinkNine = '#d3d3d3';
export const resendOtpBtnColor = '#0bd2cc';
export const brownGrey = '#a4a4a4';
export const iceBlue = '#e6f1f1';
export const transparent = '#00000000';
export const burple = '#9035d9';
export const purpleBlue = '#8928d7';
export const whiteTwo = '#f1f1f1';

export const veryLightBrown = '#d3be87';
export const muddyBrown = 'rgb(128, 94, 8)';
export const ocher = 'rgb(206, 152, 14)';
export const pooBrown = 'rgb(134, 93, 0)';
export const darkTan = '#b89845';
export const greenBlue = '#07a29d';
export const sunflowerYellow = '#ffd900';
export const duckEggBlue = '#e1fcfb';
export const whiteSix = '#f0f0f0';
export const goldenBrown = '#b58200';
export const aqua = '#0cd9d3';
export const aqua20 = 'rgba(12,217,211,0.2)';
export const aquaBlue = '#04ddd4';
export const deepBlue = '#06047d';
export const brownGrey5 = '#a3a3a30d';
export const black99 = '#000000fc';
export const veryLightPinkFive = '#dfdfdf';
export const veryLightPinkFive20 = 'rgba(223,223,223,0.2)';
export const veryLightPinkEight = '#f2f2f2';
export const whiteThree = '#eaeaea';
export const red = '#fc0b0b';
export const aquamarine = '#0bd2cc';
export const whiteFour = '#f4f4f4';
export const whiteFive = '#e8e8e8';
export const tangerine = '#ff8d00';
export const greyishBrown = '#4c4c4c';
export const redemptionLabelRed = '#e02020';
export const pdpReviewFilterBgColor = '#f5f5f5';
export const whiteSeven = '#ffffff';
export const whiteSevenR = 'rgba(214,214,214,1)';
export const whiteSeven50 = 'rgba(255,255,255,0.5)';
export const turquoise = '#00b3b2';
export const parchment = '#fff3ae';
export const deepOrangeTwo = '#e85d00';
export const blueGreen = '#0c91a1';
export const warmGreyThree = '#848484';
export const brownGreyTwo = '#a4a4a4';
export const greyishBrownTwo = '#3e3e3e';
export const brownGreyFive = 'rgba(139,139,139,1)';
export const vibrantPurple = '#c400c9';
export const cobaltBlue = '#0014a4';
export const shockingPink = '#fc00a4';
export const indigo = '#28077c';
export const whiteEight = 'rgba(247,247,247,1)';
export const aquaBlueTwo = '#00dad3';
export const azure = '#03b1f3';
export const darkCream = '#ffea8d';
export const bubblegumPink = '#ff96c7';
export const cyan = '#04fff7';
export const darkCyan = '#088a86';
export const sky = '#7adaff';
export const purplishRed = '#bb075a';
export const deepSeaBlue = '#005c7f';
export const deepAqua = '#057b77';
export const uglyYellowTwo = '#cdd500';
export const strongPinkThree = '#ff0077';
export const brightSkyBlue = '#00c6ff';
export const blueGreenTwo = '#0fad9c';
export const frogGreen = '#60cd10';
export const yellowOrange = '#f7b500';
export const brownGreyFour = 'rgba(151,151,151,0.5)';
export const hotPink = 'rgb(255,0,131)';
export const tomato = 'rgb(228,35,19)';
export const lightCyan = '#b5f3f1';
export const offWhite = '#fff9e9';
export const purplishRedTwo = '#ba0e64';
export const indigoTwo = '#4a0085';
export const veryLightGrey = '#e0e0e0';
export const hotPinkTwo = '#ff0077';
export const expressGoWhite = '#024B54';
export const brownGreyThree = 'rgba(155,155,155,1)';
export const wine = 'rgb(129,4,62)';
export const peachyPink = 'rgb(225,162,154)';
export const yellowishOrange = 'rgb(255,168,16)';
export const lightKhaki = 'rgb(242,244,185)';
export const PingFangHKRegular = 'rgb(116,116,116)';
export const lightBlue = 'rgb(213, 255, 254)';
export const redOrange = 'rgb(255,210,190)';
export const lightPurple = 'rgb(244,237,255)';
export const lightBrown = 'rgb(186,141,108)';
export const deepPurple = 'rgb(200,95,255)';
export const lightGreyTwo = 'rgb(102,102,102)';
export const lightBrownTwo = 'rgb(251,244,227)';
export const darkBlueGreen = 'rgb(12, 145, 161)';
export const yellowBrown = 'rgb(206,152,14)';
export const lightPurpleTwo = 'rgb(244,180,255)';
export const deepgrey = '#EBF0F6';
export const deepblue = '#093871';
export const lightblue = '#1CB8E2';

export const watsonGreen = '#009AA9';
export const black70 = 'rgba(0, 0, 0, 0.7)';
export const lineGrey = '#dfdfdf';
export const alipayBlue = '#009dea';
export const alipayDarkBlue = '#006ea8';

export const pink = '#EA11AE';
export const bottomBarYellow = '#F5F8DB';
export const bottomBarGreen = '#e5f9f9';
export const bottomBarOrange = '#ffedc0';

export const textGrey = '#666666';
export const watsonDarkGreen = '#045F67';
export const navBarGreen = '#c6d92c';

export const btnBagGradientLeft = '#00CAD4';
export const btnBagGradientRight = '#01A2B0';

export const btnNormalPaymentGradientLeft = '#04969F';
export const btnNormalPaymentGradientRight = '#05C7DB';

export const googlePayGrey = '#3C4043';

/**
 * text color
 */
export const text = {
  black,
  white,
  lightGrey,
  lightestGrey,
};

/**
 * background color
 */
export const background = {
  grey,
  white,
};

/**
 * tab label
 */
export const tab = {
  selected: secondary_100,
};

/**
 * zeplin
 */
export const colors = {
  greenyBlue: '#3fbdbd',
  brownGrey: '#a1a1a1',
  lightGrey: '#d5d6d2',
  tealBlue: '#009aa9',
  paleGrey: '#eff0f5',
  paleGreyTwo: '#f7f7fa',
  brownGreyTwo: '#a4a4a4',
  marine: '#032368',
  tomato: '#e42313',
  brownishGreyTwo: '#6a6a6a',
  brownishGrey: '#666666',
  veryLightPinkTwo: '#d8d8d8',
  white: '#ffffff',
  iceBlue: '#ebf7f6',
  tealBlue10: '#009aa91a',
  lightTan: '#eeeeb8',
  seafoamBlue: '#80d0d0',
  black: '#333333',
  paleBlue: '#d6ebfa',
  slateBlue: '#598091',
  brownGreyThree: '#9b9b9b',
  veryLightPink: '#dfdfdf',
  veryLightPinkThree: '#f2f2f2',
};
